package com.atlassian.marketplace.client.api;

import java.util.Optional;

import static com.atlassian.marketplace.client.api.QueryProperties.describeOptBoolean;
import static com.atlassian.marketplace.client.api.QueryProperties.describeOptEnum;
import static com.atlassian.marketplace.client.api.QueryProperties.describeParams;
import static com.atlassian.marketplace.client.util.Convert.iterableOf;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Optional.empty;

/**
 * Encapsulates search parameters that can be passed to {@link Products#find(ProductQuery)}.
 * @since 2.0.0
 */
public final class ProductQuery implements QueryProperties.ApplicationCriteria,
        QueryProperties.Bounds,
        QueryProperties.Cost,
        QueryProperties.Hosting,
        QueryProperties.WithVersion
{
    private static final ProductQuery DEFAULT_QUERY = builder().build();
    
    private final QueryBuilderProperties.ApplicationCriteriaHelper app;
    private final Optional<Cost> cost;
    private final Optional<HostingType> hosting;
    private final boolean withVersion;
    private final QueryBounds bounds;
    
    /**
     * Returns a new {@link Builder} for constructing a ProductQuery.
     */
    public static Builder builder()
    {
        return new Builder();
    }

    /**
     * Returns a ProductQuery with no criteria, which will match any available product.
     */
    public static ProductQuery any()
    {
        return DEFAULT_QUERY;
    }
    
    /**
     * Returns a new {@link Builder} for constructing a ProductQuery based on an existing ProductQuery.
     */
    public static Builder builder(ProductQuery query)
    {
        Builder builder = builder()
            .application(query.safeGetApplication())
            .appBuildNumber(query.safeGetAppBuildNumber())
            .cost(query.safeGetCost())
            .hosting(query.safeGetHosting())
            .withVersion(query.isWithVersion())
            .bounds(query.getBounds());

        return builder;
    }

    private ProductQuery(Builder builder)
    {
        app = builder.app;
        cost = builder.cost;
        hosting = builder.hosting;
        withVersion = builder.withVersion;
        bounds = builder.bounds;
    }

    @Override
    public Optional<ApplicationKey> safeGetApplication()
    {
        return app.application;
    }

    @Override
    public Optional<Integer> safeGetAppBuildNumber()
    {
        return app.appBuildNumber;
    }

    @Override
    public Optional<Cost> safeGetCost()
    {
        return cost;
    }

    @Override
    public Optional<HostingType> safeGetHosting()
    {
        return hosting;
    }

    @Override
    public boolean isWithVersion()
    {
        return withVersion;
    }
    
    @Override
    public QueryBounds getBounds()
    {
        return bounds;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String toString()
    {
        return describeParams("ProductQuery",
            app.describe(),
            describeOptEnum("cost", iterableOf(cost)),
            describeOptEnum("hosting", iterableOf(hosting)),
            describeOptBoolean("withVersion", withVersion),
            bounds.describe()
        );
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof ProductQuery) ? toString().equals(other.toString()) : false;
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
    
    /**
     * Builder class for {@link ProductQuery}.  Use {@link ProductQuery#builder()} to create an instance. 
     */
    public static class Builder implements QueryBuilderProperties.ApplicationCriteria<Builder>,
        QueryBuilderProperties.Bounds<Builder>,
        QueryBuilderProperties.Cost<Builder>,
        QueryBuilderProperties.Hosting<Builder>,
        QueryBuilderProperties.WithVersion<Builder>
    {
        private QueryBuilderProperties.ApplicationCriteriaHelper app = new QueryBuilderProperties.ApplicationCriteriaHelper();
        private Optional<Cost> cost = empty();
        private Optional<HostingType> hosting = empty();
        private boolean withVersion = false;
        private QueryBounds bounds = QueryBounds.defaultBounds();

        /**
         * Returns an immutable {@link ProductQuery} based on the current builder properties.
         */
        public ProductQuery build()
        {
            return new ProductQuery(this);
        }

        @Override
        public Builder application(Optional<ApplicationKey> application)
        {
            app = app.application(application);
            return this;
        }

        @Override
        public Builder appBuildNumber(Optional<Integer> appBuildNumber)
        {
            app = app.appBuildNumber(appBuildNumber);
            return this;
        }

        @Override
        public Builder cost(Optional<Cost> cost)
        {
            this.cost = checkNotNull(cost);
            return this;
        }

        @Override
        public Builder hosting(Optional<HostingType> hosting)
        {
            this.hosting = checkNotNull(hosting);
            return this;
        }

        @Override
        public Builder withVersion(boolean withVersion)
        {
            this.withVersion = withVersion;
            return this;
        }
        
        @Override
        public Builder bounds(QueryBounds bounds)
        {
            this.bounds = checkNotNull(bounds);
            return this;
        }
    }
}

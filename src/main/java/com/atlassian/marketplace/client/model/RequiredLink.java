package com.atlassian.marketplace.client.model;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This annotation is used internally to denote required link relations in model classes.
 * @since 2.0.0
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiredLink
{
    String rel();
}
